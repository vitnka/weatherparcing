import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {


    public static void main(String[] args) throws Exception {

        try {
            File file = new File("C:/Users/Public/WeatherParsingCSV/src/weather.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            List<CurrentDay> dayList = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                String[] elements = line.split(",");
                line = reader.readLine();
                if (elements[0].charAt(0) >= '0' && elements[0].charAt(0) <= '9') {
                    dayList.add(creatingDays(elements));
                }
            }
            WeatherMethods weatherMethods = new WeatherMethods(dayList);


            File fileAnswer = new File("statistics.txt");

            FileWriter fileWriter = new FileWriter(fileAnswer);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("Средняя температура : " + weatherMethods.countAverageTemp() + "°C\n");
            bufferedWriter.write("Средняя влажность : " + weatherMethods.countAverageHumidity() + "%\n");
            bufferedWriter.write("Средняя скорость ветра : " + weatherMethods.countAverageWindSpeed() + " km/h\n");


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM, HH:mm");


            CurrentDay maxTempWeather = weatherMethods.maxTempDay();
            Date maxTempDay = maxTempWeather.getDate();
            bufferedWriter.newLine();
            bufferedWriter.write("Максимальная температура  " + maxTempWeather.getTemperature() + "°C была зафиксирована " + simpleDateFormat.format(maxTempDay));

             CurrentDay minHumidityWeather = weatherMethods.minHumidityDay();
             Date minHumDay = minHumidityWeather.getDate();
             bufferedWriter.newLine();
             bufferedWriter.write("Минимальная влажность  " + minHumidityWeather.getHumidity() + "% была зафиксирована " + simpleDateFormat.format(minHumDay));

             CurrentDay maxSpeedWeather = weatherMethods.maxWindSpeedDay();
             Date maxSpeedDay = maxSpeedWeather.getDate();
             bufferedWriter.newLine();
             bufferedWriter.write("Максимальная скорость ветра  " + maxSpeedWeather.getWindSpeed() + " km/h была зафиксирована " + simpleDateFormat.format(maxSpeedDay));

             bufferedWriter.newLine();

             bufferedWriter.write("Самое частое направление ветра: " + weatherMethods.mostFrequentWindDirection());

             bufferedWriter.close();
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    public static CurrentDay creatingDays(String[] elements) throws ParseException {

        String date = elements[0];
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmm");
        Date correctDate = simpleDateFormat.parse(date);
        Double temperature = Double.parseDouble(elements[1]);
        Double humidity = Double.parseDouble(elements[2]);
        Double windSpeed = Double.parseDouble(elements[3]);
        Double windDirection = Double.parseDouble(elements[4]);


        return new CurrentDay(correctDate,temperature,humidity,windSpeed,windDirection);
    }
}