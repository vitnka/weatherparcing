import java.util.*;

public class WeatherMethods {
    List<CurrentDay> days ;

    public WeatherMethods(List<CurrentDay> days) {
        this.days = days;
    }

    public Double countAverageHumidity() {
        double  result;
        double sum = 0.0;

        for (CurrentDay day : days) {
            sum += day.getHumidity();
        }
        result = sum / days.size();

        return result;
    }

    public Double countAverageTemp() {
        double  result;
        double sum = 0.0;

        for (CurrentDay day : days) {
            sum += day.getTemperature();
        }
        result = sum / days.size();

        return result;
    }

    public Double countAverageWindSpeed() {
        double  result;
        double sum = 0.0;

        for (CurrentDay day : days) {
            sum += day.getWindSpeed();
        }
        result = sum / days.size();

        return result;
    }

    public CurrentDay maxTempDay() {
        double maxTemp = 0.0;
        List<Double> tempList = new ArrayList<>();
        for (CurrentDay day : days) {
            tempList.add(day.getTemperature());
        }
        maxTemp = Collections.max(tempList);
        for (CurrentDay day : days) {
            if (day.getTemperature() == maxTemp) {
                return day;
            }
        }
        return null;
    }

    public CurrentDay minHumidityDay() {
        double minHumidity;
        List<Double> tempList = new ArrayList<>();
        for (CurrentDay list : days) {
            tempList.add(list.getHumidity());
        }
        minHumidity = Collections.min(tempList);
        for (CurrentDay list : days) {
            if (list.getHumidity() == minHumidity) {
                return list;
            }
        }
        return null;
    }

    public CurrentDay maxWindSpeedDay() {
        double maxWindSpeed;
        List<Double> tempList = new ArrayList<>();
        for (CurrentDay list : days) {
            tempList.add(list.getWindSpeed());
        }
        maxWindSpeed = Collections.max(tempList);
        for (CurrentDay list : days) {
            if (list.getWindSpeed() == maxWindSpeed) {
                return list;
            }
        }
        return null;
    }

    public String mostFrequentWindDirection() {

        TreeMap<String, Integer> directions = new TreeMap<>();
        int west = 0;
        int east = 0;
        int north = 0;
        int south = 0;

        for(CurrentDay day : days){
            if(day.getWindDirection() <= 45 || day.getWindDirection() > 315) north++;
            if(day.getWindDirection() > 45 && day.getWindDirection() <= 135) east++;
            if(day.getWindDirection() > 135 && day.getWindDirection() < 225) south++;
            if (day.getWindDirection() > 225 && day.getWindDirection() < 315) west++;
        }
        directions.put("West", west);
        directions.put("North", north);
        directions.put("East", east);
        directions.put("South", south);


        return directions.lastKey();
    }
}
